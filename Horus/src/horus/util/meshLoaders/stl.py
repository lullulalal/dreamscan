#!/usr/bin/python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------#
#                                                                       #
# This file is part of the Horus Project                                #
#                                                                       #
# Copyright (C) 2014-2015 Mundo Reader S.L.                             #
# Copyright (C) 2013 David Braam from Cura Project                      #
#                                                                       #
# Date: June 2014                                                       #
# Author: Jesús Arroyo Torrens <jesus.arroyo@bq.com>                    #
#                                                                       #
# This program is free software: you can redistribute it and/or modify  #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 2 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# This program is distributed in the hope that it will be useful,       #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details.                          #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with this program. If not, see <http://www.gnu.org/licenses/>.  #
#                                                                       #
# -----------------------------------------------------------------------#

"""
STL file mesh loader.
STL is the most common file format used for 3D printing right now.
STLs come in 2 flavors.
	Binary, which is easy and quick to read.
	Ascii, which is harder to read, as can come with windows, mac and unix style newlines.
	The ascii reader has been designed so it has great compatibility with all kinds of formats or slightly broken exports from tools.

This module also contains a function to save objects as an STL file.

http://en.wikipedia.org/wiki/STL_(file_format)
"""
from horus.util.meshLoaders.StlWriter import BinarySTLWriter

__author__ = "Jesús Arroyo Torrens <jesus.arroyo@bq.com>"
__license__ = "GNU General Public License v2 http://www.gnu.org/licenses/gpl.html"

import sys
import os
import struct
import numpy as np

from horus.util import model



def _loadAscii(mesh, stream):
    cnt = 0
    for lines in stream:
        for line in lines.split('\r'):
            if 'vertex' in line:
                cnt += 1
    mesh._prepareFaceCount(int(cnt) / 3)
    stream.seek(5, os.SEEK_SET)
    cnt = 0
    data = [None, None, None]
    for lines in stream:
        for line in lines.split('\r'):
            if 'vertex' in line:
                data[cnt] = line.split()[1:]
                cnt += 1
                if cnt == 3:
                    mesh._addFace(float(data[0][0]), float(data[0][1]), float(data[0][2]),
                                  float(data[1][0]), float(data[1][1]), float(data[1][2]),
                                  float(data[2][0]), float(data[2][1]), float(data[2][2]))
                    cnt = 0


def _loadBinary(mesh, stream):
    # Skip the header

    stream.read(80 - 5)
    count = struct.unpack('<I', stream.read(4))[0]

    dtype = np.dtype([
        ('n', np.float32, (3,)),
        ('v', np.float32, (9,)),
        ('atttr', '<i2', (1,))])
    data = np.fromfile(stream, dtype=dtype, count=count)

    mesh.vertexCount = 3 * count
    print "-------------------------------"
    print mesh.vertexCount
    n = np.zeros((mesh.vertexCount / 3, 9), np.float32)
    print "-------------------------------"
    print len(n)
    print "-------------------------------"
    print len(data['n'])
    n[:, 0:3] = n[:, 3:6] = n[:, 6:9] = data['n']
    mesh.normal = n.reshape(mesh.vertexCount, 3)
    mesh.vertexes = np.reshape(data['v'], (mesh.vertexCount, 3))


def loadScene(filename):
    obj = model.Model(filename)
    m = obj._addMesh()
    with open(filename, "rb") as f:
        if f.read(5).lower() == "solid":
            _loadAscii(m, f)
        else:
            _loadBinary(m, f)
        obj._postProcessAfterLoad()
        return obj


def saveScene(filename, _object):
    with open(filename, 'wb') as f:
        saveSceneStream(f, _object)


def saveSceneStream(fp, _object):
    i = 0
    mesh = _object._mesh
    facets = triangulate(mesh.vertexes)

    writer = BinarySTLWriter(fp)
    writer.add_faces(facets)
    writer.close()
#
#     # mesh._prepareFaceCount(len(facets))
#     # result_m = addFaces(mesh, facets)
#     # result_m._calculateNormals()
#     # vertexes = result_m.vertexes
#     # while i < len(vertexes) - 2:
#     #     print vertexes[i][0], vertexes[i][1], vertexes[i][2], vertexes[i + 1][0], vertexes[i][1], vertexes[i][2], \
#     #     vertexes[i + 2][0], vertexes[i + 2][1], vertexes[i + 2][2]
#     #
#     #     stream.write(struct.pack(BINARY_FACET, vertexes[i][0], vertexes[i][1], vertexes[i][2],
#     #                              vertexes[i + 1][0], vertexes[i][1], vertexes[i][2],
#     #                              vertexes[i + 2][0], vertexes[i + 2][1], vertexes[i + 2][2]))
#     #     i += 1
#     #     if i == len(vertexes) - 2:
#     #         break
#
#
# def addFaces(mesh, facets):
#     print "add %d facets" % len(facets)
#     for facet in facets:
#         add_facet(mesh, facet)
#
#     return mesh
#
#
def triangulate(vertices):
    print "triangulate!"
    n = len(vertices)
    if n == 3:
        return vertices
    elif n < 3:
        raise ValueError('not enough vertices')
    else:
        print "facet has %d vertices" % n
        facets = []
        for i in range(0, n - 2):
            # print "i= %d" % i
            facet = [vertices[i], vertices[i + 1], vertices[i + 2]]
            facets.append(facet)
        return facets
#
#
# def add_facet(mesh, face):
#     if len(face) == 3:
#         mesh._addFace(float(face[0][0]), float(face[0][1]), float(face[0][2]),
#                       float(face[1][0]), float(face[1][1]), float(face[1][2]),
#                       float(face[2][0]), float(face[2][1]), float(face[2][2]), )
#     elif len(face) > 3:
#         facets = triangulate(face)
#         addFaces(facets)
#     else:
#         raise ValueError('wrong number of vertices')
#
#
# def extrude(self, bottom, height):
#     if len(bottom) < 3:
#         raise ValueError('not a polygon')
#     else:
#         top = []
#
#         for vertice in bottom:
#             top.append([vertice[0], vertice[1], vertice[2] + height])
#
#         bottom.reverse()
#         self.add_facet(bottom)
#         bottom.reverse()
#
#         for i in range(0, len(bottom) - 1):
#             self.add_facet([bottom[i], bottom[i + 1], top[i + 1], top[i]])
#         self.add_facet([bottom[len(bottom) - 1], bottom[0], top[0], top[len(bottom) - 1]])
#
#         self.add_facet(top)
#
#
# # def write(mesh, face):
# #     stream.write(struct.pack(BINARY_FACET, *data))
#
#
# def normal(v1, v2, v3):
#     # calculate normal vector on triangle spanned by the three vertices
#     # tells in which direction the plane looks "right-thumb-rule"
#     if (len(v1) != 3) or (len(v2) != 3) or (len(v2) != 3):
#         raise ValueError('unvalid value')
#     n = crossProduct(diff(v2, v1), diff(v3, v1))
#     absolut = 0
#     for i in n:
#         if i >= 0:
#             absolut += i
#         else:
#             absolut -= i
#     if absolut == 0:
#         print "this should not have happened!"
#         return n
#     else:
#         return [n[0] / absolut, n[1] / absolut, n[2] / absolut]
#
#
# def crossProduct(a, b):
#     # calculate cross product of two threedimensional vectors
#     if (len(a) != 3) or (len(b) != 3):
#         raise ValueError('unvalid value')
#     return [a[1] * b[2] - a[2] * b[1], a[2] * b[0] - a[0] * b[2], a[0] * b[1] - a[1] * b[0]]
#
#
# def diff(v1, v2):
#     # substracting one list from another
#     if (len(v1) != 3) or (len(v2) != 3):
#         raise ValueError('unvalid value')
#     return [v1[0] - v2[0], v1[1] - v2[1], v1[2] - v2[2]]
#
#
#
#
#
#

